/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;


import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;

/**
 *
 * @author Christine
 */


public class UnitCommitmentModel {
    private final IloCplex model;
    private final IloIntVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final UnitCommitmentProblem ucp;

    public UnitCommitmentModel(UnitCommitmentProblem ucp) throws IloException {
        this.ucp = ucp;
        this.model = new IloCplex();
        
        // Creates the decision variables 
        // 1. The u_gt variables
        u = new IloIntVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                u[g-1][t-1] = model.boolVar("u_"+g+"_"+t);
            }
        }
        
        // 2. The c_gt variables
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"c_"+g+"_"+t);
            }
        }
        
        // 3. The l_t variables
        l = new IloNumVar[ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l_"+t);
        }
        
        
        // 4. The p_gt variables
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p_"+g+"_"+t);
            }
        }
        
        
        // Creates the objective function
        // 1. Creates an empty linear expression
        IloLinearNumExpr obj = model.linearNumExpr();
        // 2. Adds the start-up costs, commitment costs and production costs
        
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                obj.addTerm(1, c[g-1][t-1]);    
                obj.addTerm(ucp.getCommitmentCost(g), u[g-1][t-1]);
                obj.addTerm(ucp.getProductionCost(g), p[g-1][t-1]);
            }
            // 3. Adds the shedding costs
            obj.addTerm(ucp.getSheddingCost(t), l[t-1]);
        }
        
        // 4. Adds the objective function to the model
        model.addMinimize(obj);
        
        
        // Creates the constraints
        
        // 1. Constraints (1b)
        // We have onle constraint for each hour and generator
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                // We bring the right-hand-side to the lhs, changing sign
                lhs.addTerm(-ucp.getStartUpCost(g), u[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(+ucp.getStartUpCost(g), u[g-1][t-2]); // Note that in order to get u_g,t-1 we need to access u[g-1][t-2] (notice the -2)                
                }
                // Finally we add the constraint
                model.addGe(lhs, 0);
            }
        }
        
        // 2. Constraints (1c)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                for(int tt = t; tt <= ucp.getMinOnTimeAtT(g, t); tt++){
                    lhs.addTerm(1, u[g-1][tt-1]);
                    lhs.addTerm(-1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
        
        // 2. Constraints (1d)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int tt = t; tt <= ucp.getMinOffTimeAtT(g, t); tt++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][tt-1]);
                    lhs.addTerm(1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(-1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
        
        // 3. Constraints (1e)
        for(int t = 1; t <= ucp.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            model.addEq(lhs, ucp.getDemand(t));
        }
        
        // 4. Constraints (1f)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-ucp.getMinProduction(g), u[g-1][t-1]);
                model.addGe(lhs,0);
            }
        }
        
        // 5. Constraints (1g)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-ucp.getMaxProduction(g), u[g-1][t-1]);
                model.addLe(lhs,0);
            }
        }
        
        // 6. Constraints (1h)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                model.addLe(lhs,ucp.getRampUp(g));
            }
        }
        
        // 7. Constraints (1i)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                model.addLe(lhs,ucp.getRampDown(g));
            }
        }
        
        
    }
    
    public void solve() throws IloException{
        model.solve();
        System.out.println("Optimal objectve "+model.getObjValue());
    }
    
    public void printSolution() throws IloException{
        System.out.println("Commitment");
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            System.out.print(ucp.getGeneratorName(g));
            for(int t =1; t <= ucp.getnHours(); t++){
                System.out.print(String.format(" %4.0f ", model.getValue(u[g-1][t-1])));
            }
            System.out.println("");
        }
        System.out.println("Startup costs");
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            System.out.print(ucp.getGeneratorName(g));
            for(int t =1; t <= ucp.getnHours(); t++){
                System.out.print(String.format(" %4.2f ", model.getValue(c[g-1][t-1])));
            }
            System.out.println("");
        }
        System.out.println("Production");
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            System.out.print(ucp.getGeneratorName(g));
            for(int t =1; t <= ucp.getnHours(); t++){
                System.out.print(String.format(" %4.2f ", model.getValue(p[g-1][t-1])));
            }
            System.out.println("");
        }
    }
}
