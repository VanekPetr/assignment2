/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;

/**
 *
 * @author Christine
 */
public class MasterProblem {
    
    private final IloCplex model;
    private final IloNumVar u[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    
    /**
     * Creates the Master Problem
     * @param ucp
     * @throws ilog.concert.IloException
     */
            
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException{
        this.ucp = ucp;
        
        this.model = new IloCplex();  
        
        // 2. Creates the decision variables
        this.u = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        
        // Assigns to each position in the array an IloNumVar object 
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for( int t = 1; t<= ucp.getnHours(); t++ ){
                u[g-1][t-1] = model.boolVar();
            } 
        }
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){ 
                obj.addTerm(ucp.getCommitmentCost(g-1), u[g-1][t-1]);
            }
        }
        // 3. Adds phi
        obj.addTerm(1, phi);
        
        
        model.addMinimize(obj);
    
        // Creates the constraints
        
        // 2. Constraints (1c)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                for(int tt = t; tt <= ucp.getMinOnTimeAtT(g, t); tt++){
                    lhs.addTerm(1, u[g-1][tt-1]);
                    lhs.addTerm(-1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
        
        // 2. Constraints (1d)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int tt = t; tt <= ucp.getMinOffTimeAtT(g, t); tt++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][tt-1]);
                    lhs.addTerm(1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(-1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
        
    }
    
    /**
     * Solves the Master Problem.
     * @throws IloException 
     */
    public void solve() throws IloException{
        
        // In this way we inform Cplex that
        // we want to use the callback we define below
        model.use(new MyCallback());
        
        // Solves the problem
        model.solve();
    }
    
    private class MyCallback extends IloCplex.LazyConstraintCallback{
        
        public MyCallback() {
        }
    
        @Override
        protected void main() throws IloException {
            // 1. We start by obtaining the solution at the current node
            double[][] U = getU();
            double Phi = getPhi();
            
            // 2. We check feasibility of the subproblem 
            // 2.1 We create and solve a feasibility subproblem 
            FeasibilitySubProblem usp = new FeasibilitySubProblem(ucp,U);
            usp.solve();
            double uspObjective = usp.getObjective();
            
            // 2.2 We check if the suproblem is feasible. 
            // Remember, if the objective is zero the subproblem is feasible
            System.out.println("USP "+uspObjective);
            if(uspObjective >= 0+1e-9){
                // 2.3 If the objective is positive 
                // the subproblem is not feasible. Thus we 
                // need a feasibility cut.
                System.out.println("Generating feasibility cut");
                // 2.4 We obtain the constant and the linear term of the cut
                // from the feasibility subproblem
                       
                
                double constantTerm = usp.getCutConstant();
                IloLinearNumExpr linearTerm = usp.getCutLinearTerm((IloIntVar[][]) u);

            // 2.5 Thus we generate and add a cut to the current model.
                // Remember that the cut is constant + linearTerm <= 0.
                // Notice that we use the method add() from the LazyConstraintCallback
                // class. This method adds the cut "lazily" to the model being
                // solved. Instead, the method model.le() does not add a cut!
                // It only creates and returns an IloRange object (which models 
                // a constraint. Notice the difference between model.le()
                // and model.addLe() which we used when creating the model. 
                
                IloRange cut = model.le(linearTerm, -constantTerm);
                add(cut);

            }else{
                // 3. Since the subproblem is feasible, we check optimality
                // and verify whether we should add an optimality cut.
                
                // 3.1. First, we create and solve an optimality suproblem
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                
                // 3.2. Then we check if the optimality test is satisfied.
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // 3.3. In this case the problem at the current node
                    // is optimal.
                    System.out.println("The current node is optimal");
                }else{
                    
                    // 3.4. In this case we need an optimality cut. 
                    System.out.println("Generating optimality cut");
                    // We get the constant and the linear term from
                    // the optimality suproblem 
                    double cutConstantTerm = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut. 
                    IloRange cut = model.le(cutTerm, -cutConstantTerm);
                    add(cut);
                }
            }
        }
    
    /**
        * Returns the X component of the solution at the current B&B integer node
        * @return the X component of the current solution
        * @throws IloException 
        */
        public double[][] getU() throws IloException{
           double U[][] = new double[ucp.getnGenerators()][ucp.getnHours()];
           for(int g = 1; g<= ucp.getnGenerators() ;g++){
               for(int t = 1; t<= ucp.getnHours(); t++){
                   U[g-1][t-1] = getValue(u[g-1][t-1]);
               }
           }  
           return U;
       }
        
        public double getPhi() throws IloException{
           return getValue(phi);
        }

        private IloRange model(int i, double d) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    
        public double getObjective() throws IloException{
        return model.getObjValue();
    }
       
    
        public void printSolution() throws IloException{
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t<=ucp.getnHours(); t++){
                System.out.println("U_"+g+""+t+" = "+model.getValue((u[g-1][t-1])));
            }
        }
    }
        
        
        
        public void end(){
        model.end();
    }
        
       
}    
