/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;


/**
 *
 * @author Christine
 */
public class FeasibilitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar c[][];
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final IloNumVar v1[][];
    private final IloNumVar v2p[];
    private final IloNumVar v2n[];
    private final IloNumVar v3[][];
    private final IloNumVar v4[][];
    private final IloNumVar v5[][];
    private final IloNumVar v6[][];
    private final UnitCommitmentProblem ucp;
    private final IloRange Constraints1b[][];
    private final IloRange Constraints1e[];
    private final IloRange Constraints1f[][];
    private final IloRange Constraints1g[][];
    private final IloRange Constraints1h[][];
    private final IloRange Constraints1i[][];
    
            
    
     public FeasibilitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException {
         this.ucp = ucp;
         this.model = new IloCplex();
         
         // 2. Creates the decision variables
         
         // 2. The c_gt variables
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        // 3. The l_t variables
        l = new IloNumVar[ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
        }
        
        // 4. The p_gt variables
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        // Creates the auxiliary variables
        
        this.v1 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v1[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v2p = new IloNumVar[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
                v2p[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        this.v2n = new IloNumVar[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
                v2n[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }    
     
        this.v3 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v3[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v4 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v4[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v5 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v5[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v6 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v6[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        
        // 3. Creates the objective function
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the equation
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v1[g-1][t-1]);
            }
        } 
        
        for(int t = 1; t <= ucp.getnHours(); t++){
         obj.addTerm(1,v2p[t-1]);
        }
        
        for(int t = 1; t <= ucp.getnHours(); t++){
         obj.addTerm(1,v2n[t-1]);
        }
       
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v3[g-1][t-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v4[g-1][t-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v5[g-1][t-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v6[g-1][t-1]);
            }
        }
       
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
       
        // 4. Creates the constraints
        
        // 1. Constraints (1b)
        this.Constraints1b = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                lhs.addTerm(1, v1[g-1][t-1]);
                if(t > 1){ // Note that in order to get u_g,t-1 we need to access u[g-1][t-2] (notice the -2)                
                    Constraints1b[t-1][g-1] = model.addGe(lhs,ucp.getStartUpCost(g)*(U[g-1][t-1]-U[g-1][t-2]));
                }else{
                    Constraints1b[t-1][g-1] = model.addGe(lhs,ucp.getStartUpCost(g)*(U[g-1][t-1]));
                }  
            }
        }
            
            // 3. Constraints (1e)
            
            
            this.Constraints1e = new IloRange[ucp.getnHours()];
            
            for(int t = 1; t <= ucp.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
                lhs.addTerm(1, l[t-1]);
                lhs.addTerm(1, v2p[t-1]);
                lhs.addTerm(-1, v2n[t-1]);
                Constraints1e[t-1] = model.addEq(lhs, ucp.getDemand(t));
        }
            
             // 4. Constraints (1f)
             
        this.Constraints1f = new IloRange[ucp.getnGenerators()][ucp.getnHours()];     
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(1,v3[g-1][t-1]);
                Constraints1f[t-1][g-1] = model.addGe(lhs,ucp.getMinProduction()[g-1]*U[g-1][t-1]);
            }
        }
            
        // 5. Constraints (1g)
        this.Constraints1g = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-1, v4[g-1][t-1]);
                Constraints1g[t-1][g-1] = model.addLe(lhs,ucp.getMaxProduction()[g-1]*U[g-1][t-1]);
            }
        }    
        
        // 6. Constraints (1h)
        this.Constraints1h = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                lhs.addTerm(-1, v5[g-1][t-1]);
                Constraints1h[g-1][t-1] = model.addLe(lhs,ucp.getRampUp(g));
            }
        }
        
        
        // 7. Constraints (1i)
        
        this.Constraints1i = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                lhs.addTerm(-1, v6[g-1][t-1]);
                Constraints1i[g-1][t-1] = model.addLe(lhs, ucp.getRampDown(g));
            }
        }
         
        
     }
     
     /**
     * Solves the problem.
     * @throws IloException 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    /**
     * Returns the objective value
     * @return the objective value
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    /**
     * Returns the constant part of the feasibility cut.
     * That is, the part of the cut not dependent on x.
     * This is given by the demand constraints. 
     * @return the constant of the cut
     * @throws IloException 
     */
    
    
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                constant = constant + model.getDual(Constraints1e[t-1]) * ucp.getDemand()[t-1]
                        + model.getDual(Constraints1h[g-1][t-1]) * ucp.getRampUp()[g-1]
                        + model.getDual(Constraints1i[g-1][t-1]) * ucp.getRampDown()[g-1][t-1];
            }
        }
        return constant;
    }
    
    // Linear term:
    
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        // Define our linear variable 
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t<= ucp.getnHours(); t++){
                
                cutTerm.addTerm(model.getDual(Constraints1b[g-1][t-1])*ucp.getStartUpCost(g), u[g-1][t-1]);
                if(t > 1){
                    cutTerm.addTerm(model.getDual(Constraints1b[g-1][t-1])*(-ucp.getStartUpCost(g)), u[g-1][t-2]);
                }
                cutTerm.addTerm(model.getDual(Constraints1f[g-1][t-1])*ucp.getMinProduction()[g-1], u[g-1][t-1]);
                cutTerm.addTerm(model.getDual(Constraints1g[g-1][t-1])*ucp.getMaxProduction()[g-1], u[g-1][t-1]);
            }
        }
        return cutTerm;
    }
    
     
    public void end(){
        model.end();
    }  
    
}
