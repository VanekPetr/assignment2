/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;

/**
 *
 * @author Christine
 */



public class OptimalitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar c[][];
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final UnitCommitmentProblem ucp;
    private final IloRange Constraints1b[][];
    private final IloRange Constraints1e[];
    private final IloRange Constraints1f[][];
    private final IloRange Constraints1g[][];
    private final IloRange Constraints1h[][];
    private final IloRange Constraints1i[][];
    
    
    /**
     * Creates the LP model for the optimality subproblem
     * @param ucp
     * @param U a solution to MP
     * @throws IloException 
     */
    
    public OptimalitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException{
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"c_"+g+"_"+t);
            }
        }
        
        l = new IloNumVar[ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l_"+t);
        }
        
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p_"+g+"_"+t);
            }
        }
        
        // 3. Creates the objective function
        
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the equation
        
        for(int t = 1; t<= ucp.getnGenerators(); t++){
            obj.addTerm(ucp.getSheddingCost()[t-1],l[t-1]);
            for(int g =1; g <= ucp.getnHours(); g++){
                obj.addTerm(ucp.getProductionCost()[g-1],p[g-1][t-1]);
                obj.addTerm(1,c[g-1][t-1]);
            }
        }
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        
        // 1. Constraints (1b)
        this.Constraints1b = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                if(t > 1){
                    Constraints1b[t-1][g-1] = model.addGe(lhs,ucp.getStartUpCost(g)*(U[g-1][t-1]-U[g-1][t-2]));               
                }else{
                    Constraints1b[t-1][g-1] = model.addGe(lhs,ucp.getStartUpCost(g)*(U[g-1][t-1]));
                }
            }
        }
            
            // 3. Constraints (1e)
            
            
            this.Constraints1e = new IloRange[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            Constraints1e[t-1] = model.addEq(lhs,ucp.getDemand(t));
        }
            
             // 4. Constraints (1f)
             
        this.Constraints1f = new IloRange[ucp.getnGenerators()][ucp.getnHours()];     
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                Constraints1f[t-1][g-1] = model.addGe(lhs,ucp.getMinProduction()[g-1]*U[g-1][t-1]);
            }
        }
            
        // 5. Constraints (1g)
        this.Constraints1g = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                Constraints1g[t-1][g-1] = model.addLe(lhs,ucp.getMaxProduction()[g-1]*U[g-1][t-1]);
            }
        }    
        
        // 6. Constraints (1h)
        this.Constraints1h = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                Constraints1h[g-1][t-1] = model.addLe(lhs, ucp.getRampUp(g));
            }
        }
        
        
        // 7. Constraints (1i)
        
        this.Constraints1i = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                Constraints1i[g-1][t-1] = model.addLe(lhs, ucp.getRampDown(g));
            }
        }
    }
    
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

    
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                constant = constant + model.getDual(Constraints1e[t-1]) * ucp.getDemand()[t-1]
                        + model.getDual(Constraints1h[g-1][t-1]) * ucp.getRampUp()[g-1]
                        + model.getDual(Constraints1i[g-1][t-1]) * ucp.getRampDown()[g-1][t-1];
            }
        }
        return constant;
    }
    
    // Linear term:
    
    public IloLinearNumExpr getCutLinearTerm(IloNumVar[][] u) throws IloException {
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t<= ucp.getnHours(); t++){
                
                cutTerm.addTerm(model.getDual(Constraints1b[g-1][t-1])*(ucp.getStartUpCost(g)), u[g-1][t-1]);
                if(t > 1){
                    cutTerm.addTerm(model.getDual(Constraints1b[g-1][t-1])*(-ucp.getStartUpCost(g)), u[g-1][t-2]);
                }
                cutTerm.addTerm(model.getDual(Constraints1f[g-1][t-1])*ucp.getMinProduction()[g-1], u[g-1][t-1]);
                cutTerm.addTerm(model.getDual(Constraints1g[g-1][t-1])*ucp.getMaxProduction()[g-1], u[g-1][t-1]);
            }
        }
        return cutTerm;
    }
    
    
    public void end(){
        model.end();
    } 
   
}
